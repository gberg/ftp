#include "Ftp.h"
#include "Manager.h"
#include "Parser.h"
#include <unistd.h>

using namespace std;

int main(int argc, char *argv[]) {
    Manager client;
    if (argc > 1) {
        client.open(argc, argv);
    } 

    string args;
    while (client.isActive()) {
        cout << "ftp > ";
        getline(cin,args);
        client.processCommand(args);
    }
}
