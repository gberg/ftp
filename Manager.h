#ifndef Manager_h
#define Manager_h
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <sys/wait.h>
#include <sys/time.h>
#include "Ftp.h"
#include "Parser.h"

using namespace std;

class Manager {

public:
    
    Manager();
    ~Manager();
    void processCommand(string);
    void open(int, char*[]);
    bool isConnected();
    bool isActive();

private:
    Ftp ftp;
    Parser parser;
    int default_port;
    bool connected;
    bool active;
    
    bool commandAndResponse(string);
    bool displayResponse();
    void initMap();
    void ls(); 
    void pwd();
    void cd(string);
    void get(string);
    void put(string);
    void user(string);
    void quit();
    void close();
    bool login();
    void open(const char*, int);
    void open(string);
    void del(string);
    void reset();
};

#endif /* Manager_h */
