#include "Ftp.h"
using namespace std;

Ftp::Ftp() {
    clientSd = NULL_FD;
    dataSd   = NULL_FD;
    file     = NULL_FD;
}

Ftp::~Ftp() {
    if (clientSd != NULL_FD) {
        close(clientSd);
    }
    if (dataSd != NULL_FD) {
        close(dataSd);
    }
}

int Ftp::openClientSocket(const char server[], int port) {
    clientSd = createSocket(server, port);
    if (clientSd == NULL_FD) {
        cout << "Client socket could not connect" << endl;
        return NULL_FD;
    }
    hostname = server;
    return clientSd;
}

int Ftp::openDataSocket(int port) {
    dataSd = createSocket((const char*)hostname.c_str(), port);
    if (dataSd == NULL_FD) {
        cout << "Data socket could not connect" << endl;
        return NULL_FD;
    }
    return dataSd;
}

int Ftp::createSocket(const char ipName[], int port) {
    struct hostent* host = gethostbyname(ipName);
    if (host == NULL) {
        perror ("Cannot find hostname");
        return NULL_FD;
    }

    sockaddr_in sendSockAddr;
    bzero((char*)&sendSockAddr, sizeof(sendSockAddr));

    sendSockAddr.sin_family      = AF_INET; // Address Family Internet
    sendSockAddr.sin_addr.s_addr = inet_addr( inet_ntoa( *(struct in_addr*)*host->h_addr_list ) );
    sendSockAddr.sin_port        = htons( port );

    // open a socket
    int sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd < 0) {
        cout << "Failed to open client socket" << endl;
        return NULL_FD;
    }

    // connect
    while (connect(sd, (sockaddr*)&sendSockAddr, sizeof(sendSockAddr)) < 0);

    return sd;
}

void Ftp::closeFile() {
    close(file);
    file = NULL_FD;
}

void Ftp::closeDataSd() {
    close(dataSd);
    dataSd = NULL_FD;
}

void Ftp::closeClientSd() {
    close(clientSd);
    clientSd = NULL_FD;
}

int Ftp::sendServerCmd(string command) {
    command.append("\r\n");
    int  len = command.length();
    char buffer[len];
    bzero(buffer, sizeof(buffer));
    strcpy(buffer, command.c_str());
    return write(clientSd, buffer, sizeof(buffer));
}

int Ftp::sendFile(const char* filename) {
    ifstream file;
    file.open(filename, ifstream::in);
    char buffer[BUFF_SIZE];
    int len   = 0;
    int total = 0;
    if (!file.is_open()) {
        cout << "Error: " << filename << " is not open" << endl;
        return -1;
    } else {
        // get length of file
        file.seekg (0, file.end);
        len = file.tellg();
        file.seekg (0, file.beg);

        while (len != 0) {    
            if (len < BUFF_SIZE) {
               file.read(buffer, len);
               total += write(dataSd, buffer, len);
               break;
            } else {
                file.read(buffer, sizeof(buffer));
                total += write(dataSd, buffer, sizeof(buffer));
            }
            len -= BUFF_SIZE;
        }
    }

    file.close();
    return total;
}

int Ftp::receiveServerResponse() {
    char buffer[BUFF_SIZE]; // 8kb buffer
    bzero(buffer, sizeof(buffer));
    int len = read(clientSd, buffer, sizeof(buffer));
    if (len == 0) {
      return -1;
    }
    response = buffer;
    return len;
}

int Ftp::receiveServerDataStream(int fd) {

    struct pollfd pfd;
    pfd.fd = fd;
    pfd.events = POLLIN;
    pfd.revents = 0;

    char buffer[BUFF_SIZE];
    bzero(buffer, sizeof(buffer));
    response.clear();

    int len = 0;   // local length
    int total = 0; // total length

    len = read(fd, buffer, sizeof(buffer));
    response.append(buffer);

    while (poll(&pfd, 1, 1000) > 0) {
        bzero(buffer, sizeof(buffer));
        len = read(fd, buffer, sizeof(buffer));
        if (len == 0) {
            break;
        }
        response.append(buffer);
        total += len;
    }

    return total;
}

int Ftp::getDataPort() {
    int x = parser.getDataPortX(response);
    int y = parser.getDataPortY(response);
    int dataPort = x * 256 + y;
    return dataPort;
}

int Ftp::getReplyCode() {
    if (response.length() > 0) {
        string code = parser.getFirstString(response);
        return atoi(code.c_str());
    }
    return -1;
}

string Ftp::getHostName() {
    return hostname;
}

string Ftp::getResponse() {
    return response;
}

int Ftp::getClientSd() {
    return clientSd;
}

int Ftp::getDataSd() {
    return dataSd;
}

