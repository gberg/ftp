This project must be compiled using C++11. This is becuase some convienient functions such as stoi() are utilized.

There is a shell script (compile.sh) included that will force C++11 compilation. Simply run './compile.sh'.

There is also an executable (ftp) included. To run this enter './ftp'.
