#ifndef Ftp_h
#define Ftp_h

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/types.h>    // socket, bind
#include <sys/socket.h>   // socket, bind, listen, inet_ntoa
#include <sys/poll.h>     // poll
#include <sys/types.h>    // file stuff
#include <sys/stat.h>     // file stuff
#include <fcntl.h>        // funct cntrl
#include <netinet/in.h>   // htonl, htons, inet_ntoa
#include <arpa/inet.h>    // inet_ntoa
#include <netdb.h>        // gethostbyname
#include <unistd.h>       // read, write, close
#include <string.h>       // bzero
#include <netinet/tcp.h>  // TCP_NODELAY
#include <stdlib.h>
#include "Parser.h"

using namespace std;
class Ftp {

#define NULL_FD -1
#define BUFF_SIZE 8192

public:
    Ftp();
    ~Ftp();
    
    int openClientSocket(const char server[], int port);
    int openDataSocket(int port);
    int getReplyCode();
    int getClientSd();
    int getDataSd();
    int getDataPort();
    int sendServerCmd(string command);
    int sendFile(const char* filename);
    int receiveServerResponse();
    int receiveServerDataStream(int fd);
    void closeFile();
    void closeDataSd();
    void closeClientSd();
    string getHostName();
    string getResponse();
    

private:
    Parser parser;
    int createSocket(const char ipName[], int port);
    int clientSd;
    int dataSd;
    int file;
    string hostname;
    string response;

};
#endif /* Ftp.h */
