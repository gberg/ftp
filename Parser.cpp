#include "Parser.h"

using namespace std;

Parser::Parser() { }

Parser::~Parser() { }

// will only split on spaces
vector<string> Parser::split(string str) {
    string buf;
    stringstream ss(str);
    vector<string> tokens;

    while (ss >> buf) {
        tokens.push_back(buf);
    }
    return tokens;
}

string Parser::getFirstString(string str) {
    vector<string> tokens = split(str);
    if (!tokens.empty()) {
        return tokens.front();
    } else {
        return "";
    }
}

string Parser::getSecondString(string str) {
    vector<string> tokens = split(str);
    if (tokens.size() == 2) {
        return tokens[1];
    } else {
        return "";
    }
}

string Parser::getLastString(string str) {
    vector<string> tokens = split(str);
    return tokens.back();
}

// 227 Entering Passive Mode (209,202,252,54,188,76)     
int Parser::getDataPortX(string response) {
    int x;
    char* c;
    
    string nums = getLastString(response);
    replaceChars(nums, ',', ' '); // need spaces to tokenize

    vector<string> numVec = split(nums);
    numVec.pop_back(); // pop off last 
    string target = numVec.back();
    
    x = atoi(target.c_str());
    return x;
}

int Parser::getDataPortY(string response) {
    int y;
    char *c;

    string nums = getLastString(response);
    replaceChars(nums, ',', ' '); // need spaces to tokenize

    string target = getLastString(nums);
    target.erase(target.end() - 1, target.end());  // strip closing paren
    
    y = atoi(target.c_str());
    return y;
}

void Parser::replaceChars(string &s, char target, char replacement) {
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == target) {
            s[i] = replacement;
        }
    }
}




