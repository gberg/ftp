#ifndef Parser_h
#define Parser_h
#include <string>
#include <cstring>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <iostream>

using namespace std;

class Parser {

public: 
    Parser();
    ~Parser();
    vector<string> split(string);
    string getFirstString(string);
    string getSecondString(string);
    string getLastString(string);
    int getDataPortX(string);
    int getDataPortY(string);

private:
    void replaceChars(string&, char, char);

};

#endif /* Parser_h */

