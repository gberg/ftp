#include "Manager.h"

using namespace std;

enum CommandVal { invalid, ls_val, put_val, get_val, close_val, quit_val, cd_val, open_val, del_val, pwd_val, user_val };

map<string,CommandVal> cmd_map;

Manager::Manager() {
    default_port = 21;
    initMap();
    connected = false;
    active = true;
}

Manager::~Manager() { }

void Manager::processCommand(string args) {
    string cmd = parser.getFirstString(args);
    
    switch(cmd_map[cmd]) {
        case cd_val    : cd(args);
                         break;
        case ls_val    : ls();
                         break;
        case get_val   : get(args);
                         break;
        case put_val   : put(args);
                         break;
        case close_val : close();
                         break;
        case quit_val  : quit();
                         break;
        case open_val  : open(args);
                         break;
        case del_val   : del(args);
                         break;
        case pwd_val   : pwd();
                         break;
        case user_val  : user(args);
                         break;
        default        : cout << "command not recognized" << endl;
                         break;
    }
}

void Manager::open(int argc, char* argv[]) {
    if (argc == 2) {
        open(argv[1],default_port);
    } else if (argc == 3) {
        open(argv[1], stoi(argv[2]));
    } else {
        cout << "Error: cannot parse args" << endl;
        return;
    }
}

void Manager::open(string args) {
    vector<string> tokens = parser.split(args);
    string open_args;
    string hostname;
    int port;
    if (tokens.size() == 1) {
        cout << "(to) ";
        getline(cin, open_args);
        if (open_args == "") {
            cout << "usage: open hostname [port]" << endl;
            return;
        } else {
            tokens = parser.split(open_args);
            if (tokens.size() == 1) { // if one arg use default port
                hostname = tokens[0];
                port = default_port;
            } else if (tokens.size() == 2) {
                hostname = tokens[0];
                port = stoi(tokens[1]);
            }
        }
    } else if (tokens.size() == 2) {
        hostname = tokens[1];
        port = default_port;
    } else if (tokens.size() == 3) {
        hostname = tokens[1];
        port = stoi(tokens[2]);
    } else {
        cout << "Error: cannot parse args" << endl;
        return;
    }
    
    open(hostname.c_str(), port);
}

void Manager::open(const char* hostname, int port) {
    ftp.openClientSocket(hostname, port);
    if(!displayResponse()) {
        return;
    }
    connected = true;
    login();
}

void Manager::user(string args) {

    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }

    vector<string> tokens = parser.split(args);
    string username;
    string password;
    if (tokens.size() == 1) {
        cout << "(username) ";
        getline(cin, username);
    } else if (tokens.size() == 2) {
        username = tokens[1];
    } else {
        cout << "Cannot parse args" << endl;
        return;
    }

    if (username  == "") {
        cout << "usage: username" << endl;
        return;
    }

    string user = "USER ";
    string command = user + username;
    if (!commandAndResponse(command)) {
        return;
    }
    int replyCode = ftp.getReplyCode();

    if (replyCode == 331) {
        cout << "Password: ";
        string password;
        cin >> password;
        string pass = "PASS ";
        command = pass + password;
        ftp.sendServerCmd(command);

        int v = ftp.receiveServerDataStream(ftp.getClientSd());
        if (v > 0) {
            cout << ftp.getResponse();
        } else {
            cout << ftp.getResponse();
            cout << "ftp: Login failed" << endl;
            return;
        }
        string syst = "SYST";
        ftp.sendServerCmd(syst);
        if(!displayResponse()) {
            return;
        }

    } else {
        cout << "ftp: Login failed" << endl;
        return;
    }
}


void Manager::ls() {

    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }

    string pasv = "PASV";
    if(!commandAndResponse(pasv)) {
        return;
    }

    int dataPort = ftp.getDataPort();
    ftp.openDataSocket(dataPort);

    // parent sends LIST
    if (!commandAndResponse("LIST")) {
        return;
    }

    // child polls
    if (fork() == 0) {
        ftp.receiveServerDataStream(ftp.getDataSd());
        cout << ftp.getResponse();
        ftp.closeDataSd();
        exit(0);
    } else {
        wait(0);
    }
    if(!displayResponse()) {
        return;
    }
}

void Manager::cd(string args) {
    
    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }
    
    vector<string> tokens = parser.split(args);
    string dir;
    if (tokens.size() == 1) {
        cout << "(remote-directory) ";
        getline(cin, dir);
    } else if (tokens.size() >= 2) {
        dir = tokens[1];
    } else {
        cout << "Cannot parse args" << endl;
        return;
    }

    if (dir == "") {
        cout << "usage: cd remote-director" << endl;
        return;
    }
    string cwd = "CWD ";
    string command = cwd + dir;
    if (!commandAndResponse(command)) {
        return;
    }
}

void Manager::get(string args) {

    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }

    // arg parsing
    vector<string> tokens = parser.split(args);
    string local_file;
    string remote_file;
    if (tokens.size() == 1) {
        cout << "(remote file) ";
        getline(cin, remote_file);
        if (remote_file == "") {
            cout << "usage: get remote-file [local-file]" << endl;
            return;
        }
        cout << "(local file) ";
        getline(cin, local_file);
        if (local_file == "") {
            cout << "usage: get remote-file [local-file]" << endl;
            return;
        }
    } else if (tokens.size() == 2) {
        remote_file = tokens[1];
        local_file = remote_file;
    } else if (tokens.size() == 3) {
        remote_file = tokens[1];
        local_file = tokens[2];
    } else {
        cout << "Error: cannot parse args" << endl;
        return;
    }

    /* for getting execution time 
    struct timeval start, end;
    gettimeofday(&start, NULL);
    */

    if (!commandAndResponse("PASV")) {
        return;
    }

    int dataPort = ftp.getDataPort();
    ftp.openDataSocket(dataPort);

    if (!commandAndResponse("TYPE I")) {
        return;
    }
    
    string retr = "RETR ";
    string command = retr + remote_file;
    
    if (!commandAndResponse(command)) {
        return;
    }
    
    if (ftp.getReplyCode() != 150) {
        return;
    }

    if (fork() == 0) {
        ftp.receiveServerDataStream(ftp.getDataSd());

        ofstream file;
        file.open(local_file, ofstream::out);

        file << ftp.getResponse();
        file.close();

        ftp.closeFile();
        ftp.closeDataSd();
        exit(0);
    } else {
        wait(NULL);
    }

    /* for getting execution time
    gettimeofday(&end, NULL);

    double time = end.tv_sec - start.tv_sec;
    time += (end.tv_usec - start.tv_usec) / 1000000;
    cout << "get execution time = " << time << " sec" << endl;
    */

    if(!displayResponse()) {
        return;
    }
}

void Manager::put(string args) {
    
    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }
   
    vector<string> tokens = parser.split(args);
    string local_file;
    string remote_file;
    if (tokens.size() == 1) {
        cout << "(local file) ";
        getline(cin, local_file);
        if (local_file == "") {
            cout << "usage: get local-file [remote-file]" << endl;
            return;
        }
        cout << "(remote file) ";
        getline(cin, remote_file);
        if (remote_file == "") {
            cout << "usage: get local-file [remote-file]" << endl;
            return;
        }
    } else if (tokens.size() == 2) {
        local_file = tokens[1];
        remote_file = local_file;
    } else if (tokens.size() == 3) {
        local_file = tokens[1];
        remote_file = tokens[2];
    } else {
        cout << "Error: cannot parse args" << endl;
        return;
    }
    
    // check if local file exists
    if (!ifstream(local_file)) {
        cout << "No such file as " << local_file << endl;
        return;
    }
    
    if (!commandAndResponse("PASV")) {
        return;
    }

    int dataPort = ftp.getDataPort();
    ftp.openDataSocket(dataPort);
    
    if (!commandAndResponse("TYPE I")) {
        return;
    }

    string stor = "STOR ";
    string command = stor + remote_file;
    
    if (!commandAndResponse(command)) {
        return;
    }
    
    int send = ftp.sendFile(local_file.c_str());
    ftp.closeDataSd();
    if (send < 0) { return; }
    
    if(!displayResponse()) {
        return;
    }
}

void Manager::del(string args) {
    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }

    vector<string> tokens = parser.split(args);
    string remote_file;
    if (tokens.size() == 1) {
        cout << "(remote-file) ";
        getline(cin,remote_file);
        if (remote_file == "") {
            cout << "usage: delete remote-file" << endl;
            return;
        }
    } else if (tokens.size() == 2) {
        remote_file = tokens[1];
    } else {
        cout << "Error: cannot parse args" << endl;
        return;
    }
    string dele = "DELE ";
    string command = dele + remote_file;
    commandAndResponse(command);
    if (ftp.getReplyCode() != 150) {
        return;
    }

    if(!displayResponse()) {
        return;
    }
}

void Manager::pwd() {
    if (!connected) {
        cout << "Not connected" << endl;
        return;
    }

    if (!commandAndResponse("PWD")) {
        return;
    }
}

bool Manager::login() {
    cout << "Name (" + ftp.getHostName() + "): ";
    string username;
    cin >> username;
    string user = "USER ";
    string command = user + username;
    if (!commandAndResponse(command)) {
        return false;
    }
    int replyCode = ftp.getReplyCode();

    if (replyCode == 331) {
        cout << "Password: ";
        string password;
        cin >> password;
        string pass = "PASS ";
        command = pass + password;
        ftp.sendServerCmd(command);

        int v = ftp.receiveServerDataStream(ftp.getClientSd());
        if (v > 0) {
            cout << ftp.getResponse();
        } else {
            cout << ftp.getResponse();
            cout << "ftp: Login failed" << endl;
            return false;
        }
        string syst = "SYST";
        ftp.sendServerCmd(syst);
        if(!displayResponse()) {
            return false;;
        }

    } else {
        cout << "ftp: Login failed" << endl;
        return false;
    }

    return true;
}

void Manager::initMap() {
    cmd_map["ls"] = ls_val;
    cmd_map["cd"] = cd_val;
    cmd_map["pwd"] = pwd_val;
    cmd_map["get"] = get_val;
    cmd_map["put"] = put_val;
    cmd_map["open"] = open_val;
    cmd_map["quit"] = quit_val;
    cmd_map["user"] = user_val;
    cmd_map["close"] = close_val;
    cmd_map["delete"] = del_val;
}

void Manager::quit() {
    if (!commandAndResponse("QUIT")) {
        return;
    }
    active = false;
}

void Manager::close() {
    if (connected) {
        if (!commandAndResponse("QUIT")) {
            return;
        }
        ftp.closeClientSd();
        connected = false;
    } else {
        cout << "Not connected" << endl; 
    }
}

// reset for server crash
void Manager::reset() {
    ftp.closeClientSd();
    connected = false;
}

bool Manager::displayResponse() {
    ftp.receiveServerResponse();
    cout << ftp.getResponse();
    if(ftp.getReplyCode() == 421) {
        reset();
        return false;
    }
    return true;
}

bool Manager::commandAndResponse(string command) {
    ftp.sendServerCmd(command);
    ftp.receiveServerResponse();
    cout << ftp.getResponse();
    if(ftp.getReplyCode() == 421) {
        reset();
        return false;
    }
    return true;
}

bool Manager::isConnected() {
    return connected;
}

bool Manager::isActive() {
    return active;
}

